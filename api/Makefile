
LK_SOURCES_DIR = ./node_modules/lenderkit-webapp/
LK_PACKAGE_DIR = /var/www/html/node_modules/lenderkit-webapp-api
LK_CORE_DIR = ../lenderkit/api

ifneq "$(wildcard ./lenderkit/ )" ""
	# mount directory with core and modules exists, adjust composer.json
	INSTALLATION_MODE = dev
	LK_SOURCES_DIR = ./lenderkit/api
endif

info:
	@echo "LenderKit WebApp Server Configuration"
	@echo " "
	@echo "Usage:"
	@echo "	make command"
	@echo " "
	@echo "Options:"
	@echo "	NONE_INTERACTIVE=1	Use it in CI to prevent errors with docker-compose exec\run"
	@echo " "
	@echo "Available commands:"
	@echo "	token		Generate API token"

############################################
# Make Targets
############################################

devmode-init:
	@if [ '$(INSTALLATION_MODE)' = 'dev' ]; then \
    		echo 'Developer mode: prepare core dev package'; \
    		if [ -d ${LK_PACKAGE_DIR} ]; then rm -rf ${LK_PACKAGE_DIR}; fi;\
    		echo "ln -s ${LK_CORE_DIR} ${LK_PACKAGE_DIR}"; \
    		ln -s ${LK_CORE_DIR} ${LK_PACKAGE_DIR}; \
    		echo "    symlink created."; \
	fi

dotenv-init:
	@echo 'Create .env file if not exists...';
	@if [ ! -f '.env' ]; then echo 'Copying .env file...'; cp .env.example .env; fi;

install: dotenv-init
	@cat .env | sed -e s/WEBAPP_SERVER_TOKEN=$$/WEBAPP_SERVER_TOKEN=$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)/g > .env
	@cat .env | grep WEBAPP_SERVER_TOKEN | sed -e "s/WEBAPP_SERVER_TOKEN=/Your token is /g"
	npm install
	$(MAKE) devmode-init

update:
	@if [ -z '$(SKIP_VENDORS)' ]; then \
		npm install; \
		$(MAKE) devmode-init; \
	else \
		echo 'SKIP_VENDORS is active. Skipping npm install...'; \
	fi
